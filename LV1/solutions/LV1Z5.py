

datoteka = input('Upisi ime datoteke: ')
lista_pouzdanosti = []
prosjek_pouzdanosti = 0

fhand = open(datoteka)
for line in fhand:   
    line = line.rstrip( )   
    if line.startswith('X-DSPAM-Confidence:'):
        lista_pouzdanosti.append(float(line[20:26]))       

prosjek_pouzdanosti = sum(lista_pouzdanosti)/len(lista_pouzdanosti)
print('Prosjek pouzdanosti u datoteci je: ' + str(prosjek_pouzdanosti))
