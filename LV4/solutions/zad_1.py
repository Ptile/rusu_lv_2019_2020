import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

#dobivanje funkcijske ovisnosti y podataka o x podatcima
def non_func(x):
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	return y

#dodavanje pogreške u stvarnim mjerenjima
def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

#izrada podataka od 1 do 10 gdje ih je 100 između
x = np.linspace(1,10,100)
#izrada izlaznih podataka y_true za x array
y_true = non_func(x)
#dodavanje šuma na stvarne podatke
y_measured = add_noise(y_true)

#plottanje prvog  grafa
plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno')
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)


np.random.seed(45)
#generiranje broja indexa
indeksi = np.random.permutation(len(x))
#izdvajanje indexa trening skupa podataka/70% podataka
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
#izdvajanje indexa testnog skupa podataka/30% podataka
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

#dodavanje indexa i oznacavanje trening skupa podataka
xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]

#dodavanje indexa i oznacavanje test skupa podataka
xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

#plottanje trening i test skupa podataka
plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

#pozivanje linearnog modela za linearnu regresiju
#i računanje dva koeficijenta za fittanje podataka
linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)

#ispis dobivenih parametara linearModel.fit(xtrain,ytrain) funkcije
print('Model je oblika y_hat = Theta0 + Theta1 * x')
print('y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x')

#testiranje linearnog modela na testnom skupu podataka
ytest_p = linearModel.predict(xtest)
#usporedba predictanih podataka i stvarnih podataka
MSE_test = mean_squared_error(ytest, ytest_p)

#plottanje predicted i stvarnog skupa podataka
plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)

#plottanje pravca koji prolazi kroz predictane podatke
x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)
