import numpy as np 
import matplotlib.pyplot as plt 

def non_func(x):
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y 

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y) 
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y)) 
    return y_noisy 

x = np.linspace(1,10,100) 
y_true = non_func(x) 
y_measured = add_noise(y_true) 

np.random.seed(12) 
indeksi = np.random.permutation(len(x)) 
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))] 
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)] 

x = x[:, np.newaxis] 
y_measured = y_measured[:, np.newaxis] 

xtrain = x[indeksi_train] 
ytrain = y_measured[indeksi_train] 

xtest = x[indeksi_test] 
ytest = y_measured[indeksi_test]

#gradijent
duljina=len(xtrain)
def pravac(x, theta0, theta1):
    return theta0 + theta1*x

alfa = 0.01
theta = [0,0]
spust=[]

for i in range(0,5000):
    sumat1=0
    sumat2=0
    udaljenost=0
    
    for j in range (0, duljina):
        sumat1= sumat1 + (pravac(xtrain[j], theta[0], theta[1])-ytrain[j])*xtrain[j]/len(xtrain)
        sumat2= sumat2 + (pravac(xtrain[j], theta[0], theta[1])-ytrain[j])/len(xtrain);
        udaljenost=udaljenost + abs(ytrain[j]-theta[0]-theta[1]*xtrain[j])
        
    spust.append(udaljenost)
    theta[0] =theta[0] - alfa*sumat2;    
    theta[1] =theta[1] - alfa*sumat1;

        
plt.figure(1)
plt.plot(spust[:8])
print ('Model je oblika y_hat = Theta0 + Theta1 * x')
print ('y_hat = ', theta[0], '+', theta[1], '*x')

#theta je ista na kraju kao i u prvom zadatku