import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')

#BARPLOT
fig = plt.figure(1)
ax = fig.add_axes([0,0,5,2])
ax.bar(mtcars[mtcars.cyl == 4].car, mtcars[mtcars.cyl == 4].mpg, color='b', width = 0.25)
ax.bar(mtcars[mtcars.cyl == 6].car, mtcars[mtcars.cyl == 6].mpg, color='r', width = 0.25)
ax.bar(mtcars[mtcars.cyl == 8].car, mtcars[mtcars.cyl == 8].mpg, color='g', width = 0.25)
ax.legend(labels=['auti s 4 cilindra', 'auti s 6 cilidara', 'auti s 8 cilindara'])
fig.show()
#BOXPLOT
fig = plt.figure(2) 
ax = fig.add_axes([0, 0, 1, 1]) 
cyls = [mtcars[mtcars.cyl==4].wt, mtcars[mtcars.cyl==6].wt, mtcars[mtcars.cyl==8].wt]
bp = ax.boxplot(cyls) 
ax.set_title("Ovisnost broja cilindara o tezini automobila")
ax.set_ylabel("Tezina automobila(lbs)")
ax.set_xlabel("Grupa automobila")
ax.set_xticklabels(['4 cilindra','6 cilindara','8 cilindara'])
fig.show()
#Usporedba potrosnje automatskog i rucnog mjenjaca
fig = plt.figure(3)
ax = fig.add_axes([0,0,1,1])
shifts = [mtcars[mtcars.am == 1].mpg, mtcars[mtcars.am == 0].mpg]
bp = ax.boxplot(shifts)
ax.set_title("Ovisnost potrosnje o vrsti mjenjaca")
ax.set_ylabel("Potrosnja(miles/galon)")
ax.set_xlabel("Vrsta mjenjaca")
ax.set_xticklabels(['Rucni','Automatski'])
fig.show()
#ODGOVOR = Rucni mjenjac trosi vise nego automatski mjenjac
#Odnos ubrzanja i snage automobila za rucne i automatske mjenjace
plt.figure(4)
plt.scatter(mtcars[mtcars.am==1].hp, mtcars[mtcars.am==1].mpg, color='r' )
plt.scatter(mtcars[mtcars.am==0].hp, mtcars[mtcars.am==0].mpg, color='b')
plt.xlabel('Potrosnja (miles/galon)')
plt.ylabel('Ubrzanje (konjske snage)')
plt.legend(['Rucni mjenjac','Automatski mjenjac'])