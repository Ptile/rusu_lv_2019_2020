import urllib
import pandas as pd
import xml.etree.ElementTree as ET

# url that contains valid xml file:
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=01.1.2017&vrijemeDo=01.12.2017'

airQualityHR = urllib.request.urlopen(url).read()
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1


df.vrijeme = pd.to_datetime(df.vrijeme, utc=True)
df.plot(y='mjerenje', x='vrijeme');

# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek

#Tri datuma kada je koncentracija PM10 bila najveca
sortirani_podaci = df.sort_values("mjerenje")
print('Tri dana s najvecim koncentracijama P10 cestica su: \n',sortirani_podaci[['mjerenje','vrijeme','dayOfweek','month']].tail(3))

#Izostali dani iz kalendara
izostali_dani = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31] - df.groupby("month").mjerenje.count()
izostali_dani.plot.bar()

#Boxplot usporedba ljetnog i zimskog mjeseca
Usporedba = df[(df.month == 11) | (df.month == 7)]
Usporedba.boxplot(column = ['mjerenje'], by = 'month')

#Distribucija PM10 cestica tijekom dana i vikendom
df['Vikend'] = ((df.dayOfweek >= 5) & (df.dayOfweek <= 6))
df.boxplot(column = ['mjerenje'], by='Vikend')












