import numpy as np
import pandas as pd


mtcars = pd.read_csv('../resources/mtcars.csv') 


#5 s najvecom potrosnjom
mtcars.sort_values(by=['mpg'], inplace=True)
print(mtcars.car.tail(5)) 

#3 automobila s 8 cilindara s najmanjom potrosnjom
mtcars.sort_values(by=['mpg'], inplace=True)
print(mtcars[(mtcars.cyl == 8)].car.head(3))

#srednja potrosnja automobila s 6 cilindara
new_mtcars = mtcars[mtcars.cyl == 6]
print('Srednja vrijednost potrosnje je: ',new_mtcars.mpg.mean())

#srednja potrosnja s 4 cilindra mase između 2000 i 2200 lbs
new_mtcars = mtcars[(mtcars.cyl == 4) & (mtcars.wt >= 2.0) & (mtcars.wt <= 2.2)]
print('Srednja vrijednost potrosnje je: ',new_mtcars.mpg.mean())

#koliko je automobila s rucnim a koliko s automatskim mjenjacem
manual_transmission = mtcars[mtcars.am == 1]
print('Automobila s manual transmissionom ima: ', manual_transmission.car.count())
print('Automobila s automatic transmissionom ima:', mtcars.car.count()-manual_transmission.car.count())


#automobila s automatskim mjenjacem i snagom preko 100 konjskih snaga
new_mtcars = mtcars[(mtcars.am == 1)&(mtcars.hp > 100)]
print('Automobila s automatskim mjenjacem i preko 100 konjskih snaga ima: ', new_mtcars.car.count())

#masa automobila u kilogramima
#mtcars['kg'] = np.ones(len(mtcars),dtype = float)
mtcars['kg'] = mtcars["wt"] * 0.45359237 * 1000

print(mtcars[['car','kg']]) 