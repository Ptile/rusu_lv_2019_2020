import numpy as np
from sklearn.datasets import fetch_openml
import sklearn.externals
import joblib
import pickle
from sklearn.neural_network import MLPClassifier as mlpc
from sklearn.metrics import plot_confusion_matrix
import matplotlib.pyplot as plt

mnist = fetch_openml('mnist_784')
X, y = mnist.data, mnist.target
class_names = ['0','1','2','3','4','5','6','7','8','9']
#print('Got MNIST with %d training- and %d test samples' % (len(X), len(y_test)))
#print('Image size is:')

# rescale the data, train/test split
X = X / 255.
X_train, X_test = X[:60000], X[60000:]
y_train, y_test = y[:60000], y[60000:]


# TODO: build youw own neural network using sckitlearn MPLClassifier 
mlp_mnist = mlpc(random_state=1, max_iter=300).fit(X_train, y_train)

# TODO: evaluate trained NN
mlp_mnist.predict(X_test)
print("nesto")
print("the score is: " + str(mlp_mnist.score(X_test, y_test)))

titles_options = [("Confusion matrix, without normalization", None),
                  ("Normalized confusion matrix", 'true')]
for title, normalize in titles_options:
    disp = plot_confusion_matrix(mlp_mnist, X_test, y_test,
                                 display_labels=class_names,
                                 cmap=plt.cm.Blues,
                                 normalize=normalize)
    disp.ax_.set_title(title)

    print(title)
    print(disp.confusion_matrix)

plt.show()

# save NN to disk
filename = "NN_model.sav"
joblib.dump(mlp_mnist, filename)

