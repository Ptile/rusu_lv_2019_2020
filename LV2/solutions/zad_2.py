import re
datoteka = '../resources/mbox-short.txt'
emailovi = []

fhand = open(datoteka)
for line in fhand:
    pomocna = re.findall('[a-z0-9]+[\._]*[a-z0-9]*[@]\S+', line)
    for pomocnik in pomocna:
        emailovi.append(pomocnik[:pomocnik.find('@')])

prvi = []
for email in emailovi:
    baremjedanA = re.findall('\S*a\S*', email)
    for linija in baremjedanA:
        prvi.append(linija)  
#print(prvi)   
       


drugi = []
for email in emailovi:
    tocnojedanA = re.findall('^[^a]*a[^a]*$', email) #pocetak znakovnog niza-svi osim a(moze i ne mora)-a-svi osim a(moze i ne mora)-kraj znakovnog niza
    for linija in tocnojedanA:
        drugi.append(linija)        
#print(drugi)

treci = []
for email in emailovi:
    nemaA = re.findall('^[^aA]+$', email)
    for linija in nemaA:
        treci.append(linija)
#print(treci)

cetvrti = []
for email in emailovi:
    visenumZnak = re.findall('^\S*[0-9]+\S*$',email)
    for linija in visenumZnak:
        cetvrti.append(linija)
#print(cetvrti)

peti = []
for email in emailovi:
    samomalaSlova = re.findall('^[a-z]+[.]*[a-z]*$',email)
    for linija in samomalaSlova:
        peti.append(linija)
print(peti)
