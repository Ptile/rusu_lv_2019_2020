import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

ovisnost = []

auti = pd.read_csv ('mtcars.csv')

potrosnja = auti.mpg
konjske_snage = auti.hp
tezine = auti.wt


plt.scatter(potrosnja, konjske_snage, marker='X', c=tezine)
plt.colorbar().set_label('Tezina (lbs / 1000)')
plt.ylabel('Konjske snage')
plt.xlabel('Potrosnja vozila')
plt.show()

print('Minimalna potrosnja automobila je: ', potrosnja.min())
print('Maksimalna potrosnja automobila je: ', potrosnja.max())
print('Srednja potrosnja automobila je: ', potrosnja.mean())
print(konjske_snage)