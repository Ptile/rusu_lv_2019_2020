import numpy as np
import matplotlib.pyplot as plt 

def prosjek_muski():    
    prosjek = sum(Visine[Osobe==1])/len(Visine[Osobe==1])    
    return prosjek

def prosjek_zenski():    
    prosjek = sum(Visine[Osobe==0])/len(Visine[Osobe==0])    
    return prosjek




visineOsoba = []
Wcounter =0
Mcounter = 0
Osobe = np.random.randint(2,size=1200)

Visine = np.zeros(1200, dtype=int)

Visine[Osobe == 0] = np.random.normal(168,7,len(Visine[Osobe==0]))
Visine[Osobe == 1] = np.random.normal(180,7,len(Visine[Osobe==1]))

plt.hist(Visine[Osobe==1], bins=50, color='b')
plt.hist(Visine[Osobe==0], bins=50, color='r')

plt.axvline(prosjek_muski(), color="darkblue")
plt.axvline(prosjek_zenski(), color="darkred")
