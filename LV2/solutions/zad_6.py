import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

img = mpimg.imread('tiger.png')


imgplot = plt.imshow(img)
plt.show()

matrica = np.array(img)
matrica *= 2


imgplot = plt.imshow(matrica)
plt.show()