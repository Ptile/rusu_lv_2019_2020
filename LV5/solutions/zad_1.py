from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

#prvi zadatak
dataset = generate_data(500,5)

kmeans = KMeans(n_clusters = 3, max_iter = 10)
prediction = kmeans.fit_predict(dataset)
#prikaz podataka  boji ovisno u kojem se clusteru nalaze
plt.scatter(dataset[:,0], dataset[:,1], c=kmeans.labels_.astype(float))
#prikaz pozicija centara clustera
plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s=200, c= 'red' , marker='o')
plt.show()

#mjenjanjem oblika generiranja podataka mjenjaju se polozaji podataka i oblike koji oni tvore, nikada se nece generirati
#podatci na istim mjestima ali ce biti dovoljno blizu da zadri oblik generiranja podataka kao sto su podatci  ukrugovima,
#u obliku polumjeseca i sl.


