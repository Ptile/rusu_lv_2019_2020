from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

#prvi zadatak
dataset = generate_data(500,1)

kmeans = KMeans(n_clusters = 3, max_iter = 10)
prediction = kmeans.fit_predict(dataset)
krit_fja = []
broj_clustera = [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]

for i in range(1,20):
    kmeans = KMeans(n_clusters = i)
    prediction = kmeans.fit_predict(dataset)
    km = kmeans.score(dataset)
    krit_fja.append(km)
    
plt.scatter(broj_clustera,krit_fja)